﻿using Data.Context;
using Data.Entities;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Features.Authorization.Services;
using Web.Features.Billing.Models;
using Web.Features.Billing.Services;
using static Test.Extensions.InMemoryContextExtensions;

namespace Test.Web.Features.Billing
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class BillingServiceTest
    {
        #region Setup

        private sealed class Setup : IDisposable
        {
            public Setup(MyContext dbContext)
            {
                Db = dbContext;
            }

            public MyContext Db;

            public void Dispose()
            {
                Db?.Dispose();
            }
        }

        private Setup Initialize()
        {
            var db = CreateTestContext();
            return new Setup(db);
        }

        #endregion

        [Test]
        public async Task HasAccessAsync_Should_Return_True()
        {
            // Arrange
            using var deps = Initialize();

            var billingService = new BillingService(deps.Db);

            var dbUser = User.Create("Test User", "dev@technical.tax");           
            var dbBillingTier = BillingTier.Create("Test Hobbyist", 100, 4999);
            var dbUserPurchase = Purchase.Create(2020, DateTime.Now, 4999, dbBillingTier);
            var dbUserReport = Report.Create(2020, 50, 5, 5, 100m);
            
            dbUser.Purchases.Add(dbUserPurchase);
            dbUser.Reports.Add(dbUserReport);

            deps.Db.BillingTiers.Add(dbBillingTier);
            deps.Db.Users.Add(dbUser);
            deps.Db.SaveChanges();

            var billingContext = new BillingContext(dbUser, dbUserReport);

            // Act
            var hasAccess = await billingService.HasAccessAsync(billingContext, default);

            // Assert
            hasAccess.Should().BeTrue();
        }
       
        [Test]
        public async Task HasAccessAsync_Should_Return_False()
        {
            // Arrange
            using var deps = Initialize();

            var billingService = new BillingService(deps.Db);

            var dbUser = User.Create("Test User", "dev@technical.tax");
            var dbUserReport = Report.Create(2020, 50, 5, 5, 100m);

            dbUser.Reports.Add(dbUserReport);

            deps.Db.Users.Add(dbUser);
            deps.Db.SaveChanges();

            var billingContext = new BillingContext(dbUser, dbUserReport);

            // Act
            var hasAccess = await billingService.HasAccessAsync(billingContext, default);

            // Assert
            hasAccess.Should().BeFalse();
        }

        [Test]
        public async Task CalculatePriceAsync_Should_Return_BillingTier_Price()
        {
            // Arrange
            using var deps = Initialize();

            var billingService = new BillingService(deps.Db);

            var dbBillingTierTestHobbyist = BillingTier.Create("Test Hobbyist", 100, 4999);
            var dbBillingTierTestDayTrader = BillingTier.Create("Test Day Trader", 1500, 9999);

            var dbUser = User.Create("Test User", "dev@technical.tax");
            var dbUserReport = Report.Create(2020, 100, 5, 5, 100m);

            dbUser.Reports.Add(dbUserReport);

            deps.Db.BillingTiers.Add(dbBillingTierTestHobbyist);
            deps.Db.BillingTiers.Add(dbBillingTierTestDayTrader);
            deps.Db.Users.Add(dbUser);
            deps.Db.SaveChanges();

            var billingContext = new BillingContext(dbUser, dbUserReport);

            // Act
            var price = await billingService.CalculatePriceAsync(billingContext, default);

            // Assert
            price.Should().Be(9999);
        }

        [Test]
        public void CalculatePriceAsync_Should_Throw_When_BillingTier_Not_Found()
        {
            // Arrange
            using var deps = Initialize();

            var billingService = new BillingService(deps.Db);

            var dbBillingTierTestHobbyist = BillingTier.Create("Test Hobbyist", 100, 4999);
            var dbBillingTierTestDayTrader = BillingTier.Create("Test Day Trader", 1500, 9999);

            var dbUser = User.Create("Test User", "dev@technical.tax");
            var dbUserReport = Report.Create(2020, 1500, 5, 5, 100m);

            dbUser.Reports.Add(dbUserReport);

            deps.Db.BillingTiers.Add(dbBillingTierTestHobbyist);
            deps.Db.BillingTiers.Add(dbBillingTierTestDayTrader);
            deps.Db.Users.Add(dbUser);
            deps.Db.SaveChanges();

            var billingContext = new BillingContext(dbUser, dbUserReport);

            // Act
            Func<Task> action = async () => await billingService.CalculatePriceAsync(billingContext, default);
            action.Invoke();

            // Assert
            action.Should().Throw<InvalidOperationException>();
        }
    }
}
