﻿using Web.Features.Billing.Abstractions;
using Web.Features.Billing.Models;
using System;
using System.Threading;
using System.Threading.Tasks;
using Data.Context;
using System.Linq;
using Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Web.Features.Billing.Services
{
    public sealed class BillingService : IBillingService
    {
        private readonly MyContext _db;

        public BillingService(MyContext db)
        {
            _db = db;
        }

        /// <inheritdoc/>
        public Task<bool> HasAccessAsync(BillingContext MyContext, CancellationToken cancellationToken)
        {
            //Sum of trades, incoming and outgoing transactions         
            int sumTradesTransactions = MyContext.Report.TradeCount + MyContext.Report.IncomingTxCount + MyContext.Report.OutgoingTxCount;

            //Get billing tiers for purchased report for given TaxYear and
            //filter billing tier which is satisfied by pricing rule
            var existingBillingTier = MyContext.User.Purchases
                .Where(p => p.TaxYear == MyContext.Report.TaxYear)
                .Select(p => p.Tier)
                .Where(bt => bt.Threshold >= sumTradesTransactions)
                .FirstOrDefault();

            return Task.FromResult(existingBillingTier != null);
        }

        /// <inheritdoc/>
        public async Task<int> CalculatePriceAsync(BillingContext MyContext, CancellationToken cancellationToken)
        {
            //Get all available billing tiers
            var billingTiers = GetBillingTiers(cancellationToken).Result;

            //Sum of trades, incoming and outgoing transactions         
            int sumTradesTransactions = MyContext.Report.TradeCount + MyContext.Report.IncomingTxCount + MyContext.Report.OutgoingTxCount;

            //Get billing tier which satisfied by pricing rule
            var billingTier = billingTiers
                    .Where(bt => bt.Threshold >= sumTradesTransactions)
                    .FirstOrDefault();

            if (billingTier == null)
                throw new InvalidOperationException("Billing tier not found.");

            return await Task.FromResult(billingTier.FullAmountInCents);
        }

        private async Task<IEnumerable<BillingTier>> GetBillingTiers(CancellationToken cancellationToken)
        {
            return await _db.BillingTiers
                    .AsQueryable()
                    .OrderBy(bt => bt.Threshold)
                    .ToListAsync(cancellationToken);
        }
    }
}
