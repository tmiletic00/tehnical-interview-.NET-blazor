﻿using System.ComponentModel.DataAnnotations;

namespace Web.Features.Billing.Models
{
    public class PaymentModel
    {
        [Required(ErrorMessage = "Credit card number is required field!")]
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Only numeric values are allowed!")]
        [StringLength(19, MinimumLength = 13, ErrorMessage = "Credit card number must contain 13 to 19 digits!")]
        public string CreditCardNumber { get; set; }
        [Required(ErrorMessage = "Expiration date is required field!")]
        [RegularExpression("^(0[1-9]|10|11|12)/20[0-9]{2}$", ErrorMessage = "Enter a date in MM/YYYY format!")]
        public string ExpirationDate { get; set; }
        [Required(ErrorMessage = "Security code is required field!")]
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Only numeric values are allowed!")]
        [StringLength(4, MinimumLength = 3, ErrorMessage = "Security code must contain 3 or 4 digits!")]
        public string SecurityCode { get; set; }
    }
}
