﻿using Data.Context;
using Data.Entities;
using Web.Features.Authorization.Abstractions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Web.Features.Authorization.Services
{
    public sealed class CurrentUserAccessor : ICurrentUserAccessor
    {
        private readonly MyContext _db;

        public CurrentUserAccessor(MyContext db)
        {
            _db = db;
        }

        /// <inheritdoc/>
        public async Task<User> GetUserAsync(CancellationToken cancellationToken)
        {
            var user = await _db.Users
                .AsQueryable()
                .Where(o => o.Email == MyContext.CURRENT_USER_EMAIL)
                .SingleOrDefaultAsync(cancellationToken);

            if (user is null)
                throw new InvalidOperationException("User is not authenticated.");

            return user;
        }
    }
}
